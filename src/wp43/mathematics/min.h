// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/min.h
 */
#if !defined(MIN_H)
  #define MIN_H

  #include <stdint.h>

  void fnMin(uint16_t unusedButMandatoryParameter);

#endif // !MIN_H
