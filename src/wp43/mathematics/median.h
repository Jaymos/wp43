// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

// Imported from C47

/**
 * \file mean.h
 */
#if !defined(MEDIAN_H)
  #define MEDIAN_H

  #include <stdint.h>

  void fnPercentileXY       (uint16_t unusedButMandatoryParameter);
  void fnMedianXY           (uint16_t unusedButMandatoryParameter);
  void fnMADXY              (uint16_t unusedButMandatoryParameter);
  void fnIQRXY              (uint16_t unusedButMandatoryParameter);

#endif // !MEDIAN_H
