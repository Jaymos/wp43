// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/ixyz.h
 */
#if !defined(IXYZ_H)
  #define IXYZ_H

  #include <stdint.h>

  void fnIxyz(uint16_t unusedButMandatoryParameter);

#endif // !IXYZ_H
