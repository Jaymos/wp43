// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/checkValue.h
 */
#if !defined(CHECKVALUE_H)
  #define CHECKVALUE_H

  #include <stdint.h>

  void fnCheckValue   (uint16_t mode);

  void checkValueError(uint16_t unusedButMandatoryParameter);
  void checkValueLonI (uint16_t mode);
  void checkValueRema (uint16_t mode);
  void checkValueCxma (uint16_t mode);
  void checkValueShoI (uint16_t mode);
  void checkValueReal (uint16_t mode);
  void checkValueCplx (uint16_t mode);

#endif // !CHECKVALUE_H
