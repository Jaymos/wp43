// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/mean.h
 */
#if !defined(MEAN_H)
  #define MEAN_H

  #include <stdint.h>

  void fnMeanXY             (uint16_t unusedButMandatoryParameter);
  void fnGeometricMeanXY    (uint16_t unusedButMandatoryParameter);
  void fnWeightedMeanX      (uint16_t unusedButMandatoryParameter);
  void fnHarmonicMeanXY     (uint16_t unusedButMandatoryParameter);
  void fnRMSMeanXY          (uint16_t unusedButMandatoryParameter);

#endif // !MEAN_H
