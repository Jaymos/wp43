// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file mathematics/iteration.h
 */
#if !defined(ITERATION_H)
  #define ITERATION_H

  #include <stdint.h>

  void fnIse(uint16_t regist);
  void fnIsg(uint16_t regist);
  void fnIsz(uint16_t regist);
  void fnDse(uint16_t regist);
  void fnDsl(uint16_t regist);
  void fnDsz(uint16_t regist);

#endif // !ITERATION_H
