// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

#include "wp43-dmcp.h"

#include "apps/timerApp.h"
#include "calcMode.h"
#include "charString.h"
#include "config.h"
#include "core/memory.h"
#include "flags.h"
#include "items.h"
#include "hal/time.h"
#include "hal/timer.h"
#include "longIntegerType.h"
#include "ui/keyboard.h"
#include "ui/screen.h"
#include "ui/softmenus.h"
#include "ui/statusBar.h"
#include <stdbool.h>

#include "wp43.h"

bool     backToDMCP;
uint32_t nextTimerRefresh;

// [DL - 2023/11/09] We assume that the WP43 keymap.bin has been applied and don't check anymore the keyboard layout
// bool     wp43KbdLayout;

#define M0 1   //  20 000 Hz  Read Key1 & 2
#define M1 2   //  10 000 Hz  Key Pressed
#define M2 3   //   6 666 Hz  Key Released
#define M3 4   //   5 000 Hz  Main loop start
#define M4 5   //   4 000 Hz  TEST loop start
#define M5 6   //   3 333 Hz
#define M6 8   //   2 500 Hz 
#define M7 10  //   2 000 Hz
#if defined(DEBUG_POWER)
  static void powerDebugMark(uint32_t markNumber) {
    #define basef 20000 //Hz
    sys_delay(1); //start off with a delay before the mark.
    start_buzzer_freq(30000000); // 30 kHz start pulse for identification. DMCP always breaks the first pulse or couple
    sys_delay(1);
    start_buzzer_freq(1000 * (uint32_t)((float)(basef)/(float)(markNumber + 0.0f))); //Variable frequency Mark
    sys_delay(1);
    stop_buzzer();
  }
#else
  static inline void powerDebugMark(uint32_t markNumber) {
  }
#endif // DEBUG_POWER



int convertKeyCode(int key) {

// [DL - 2023/11/09] We assume that the WP43 keymap.bin has been applied and don't check anymore the keyboard layout
//  if(!wp43KbdLayout) {
//    return key;
//  }

  // For key reassignment see:
  // https://technical.swissmicros.com/dm42/devel/dmcp_devel_manual/#_system_key_table
  //
  // Output of keymap2layout keymap.txt
  //
  //    +-----+-----+-----+-----+-----+-----+
  // 1: | F1  | F2  | F3  | F4  | F5  | F6  |
  //    |38:38|39:39|40:40|41:41|42:42|43:43|
  //    +-----+-----+-----+-----+-----+-----+
  // 2: | 1/x |Sum+ | SIN | LN  | LOG |SQRT |
  //    | 1: 2| 2: 1| 3:10| 4: 5| 5: 4| 6: 3|
  //    +-----+-----+-----+-----+-----+-----+
  // 3: | STO | RCL | RDN | COS | TAN |SHIFT|
  //    | 7: 7| 8: 8| 9: 9|10:11|11:12|12:28|
  //    +-----+-----+-----+-----+-----+-----+
  // 4: |   ENTER   |x<>y | CHS |  E  | <-- |
  //    |   13:13   |14:14|15:15|16:16|17:17|
  //    +-----------+-----+-----+-----+-----+
  // 5: |  DIV |   7  |   8  |   9  |  XEQ  |
  //    | 18:22| 19:19| 20:20| 21:21| 22: 6 |
  //    +------+------+------+------+-------+
  // 6: |  MUL |   4  |   5  |   6  |  UP   |
  //    | 23:27| 24:24| 25:25| 26:26| 27:18 |
  //    +------+------+------+------+-------+
  // 7: |  SUB |   1  |   2  |   3  | DOWN  |
  //    | 28:32| 29:29| 30:30| 31:31| 32:23 |
  //    +------+------+------+------+-------+
  // 8: |  ADD |   0  |  DOT |  RUN | EXIT  |
  //    | 33:37| 34:34| 35:35| 36:36| 37:33 |
  //    +------+------+------+------+-------+

  // The keys from DMCP are ordered from top left (excluding function keys) in rows, which is exactly the
  // order we want. However, we apply a keymap so that the keys works as expected in the DMCP menus.
  // If we have applied this keymap, we need to reverse this translation to get them back in the order we
  // expect.
  // The layout above shows the original number followed by the new assignment for each key. To reverse the
  // map we take the key codes and find the WP43 key that corresponds to the new position for that key code.
  // Since the map is to preserve meaning, this mapping has a close correspondence between the DMCP key
  // code and the WP43 key code.
  switch(key) {
    case KEY_SIGMA: return kcExp;
    case KEY_INV:   return kcInv;
    case KEY_SQRT:  return kcSqrt;
    case KEY_LOG:   return kcEToX;
    case KEY_LN:    return kcLn;
    case KEY_XEQ:   return kcXeq;
  //case KEY_STO:   return kcSto;
  //case KEY_RCL:   return kcRcl;
  //case KEY_RDN:   return kcRdown;
    case KEY_SIN:   return kcTri;
    case KEY_COS:   return kcCC;
    case KEY_TAN:   return kcShiftF;
  //case KEY_ENTER: return kcEnter;
  //case KEY_SWAP:  return kcSwap;
  //case KEY_CHS:   return kcChs;
  //case KEY_E:     return kcE;
  //case KEY_BSP:   return kcBackspace;
    case KEY_UP:    return kcUp;
  //case KEY_7:     return kc7;
  //case KEY_8:     return kc8;
  //case KEY_9:     return kc9;
    case KEY_DIV:   return kcDiv;
    case KEY_DOWN:  return kcDown;
  //case KEY_4:     return kc4;
  //case KEY_5:     return kc5;
  //case KEY_6:     return kc6;
    case KEY_MUL:   return kcMul;
    case KEY_SHIFT: return kcShiftG;
  //case KEY_1:     return kc1;
  //case KEY_2:     return kc2;
  //case KEY_3:     return kc3;
    case KEY_SUB:   return kcSub;
    case KEY_EXIT:  return kcExit;
  //case KEY_0:     return kc0;
  //case KEY_DOT:   return kcDot;
  //case KEY_RUN:   return kcRun;
    case KEY_ADD:   return kcAdd;
    default:        return key;
  }
}

//Alpha keyboard mapping for DMCP based on the DM41X example from David
// includes keycodes remapping for the WP43
// 
const char alpha_upper_transl[] = "_" // code 0 unused
//        +-------+-------+-------+-------+-------+-------+
//  1- 6  |  EXP  |  1/x  |  SQRT |  e^x  |  LN   |  XEQ  |
             "B"     "A"     "F"     "E"     "D"     "@"  
//        +-------+-------+-------+-------+-------+-------+
//  7-12  |  STO  |  RCL  |  R↓   |  TRI  |  CC   |   f   |
             "G"     "H"     "I"     "C"     "J"     "@"  
//        +-------+-------+-------+-------+-------+
// 13-17  | ENTER | x<>y  |  +/-  |   E   |  <--  |
             "@"     "K"     "L"     "M"     "@"  
//        +-------+-------+-------+-------+-------+
// 18-22  |  UP   |   7   |   8   |   9   |  DIV  |
             "@"     "O"     "P"     "Q"     "N"  
//        +-------+-------+-------+-------+-------+
// 23-27  | DOWN  |   4   |   5   |   6   |  MUL  |
             "@"     "S"     "T"     "U"     "R"  
//        +-------+-------+-------+-------+-------+
// 28-32  |   g   |   1   |   2   |   3   |  SUB  |
             "@"     "W"     "X"     "Y"     "V"  
//        +-------+-------+-------+-------+-------+
// 33-37  |  ON   |   0   |  DOT  |  R/S  |  ADD  |
             "@"     "?"     ","     " "     "Z"  ;
//The following lines from the DM41X are not included for the WP43 as 
// there is no alpha character assigned to the function keys
//        +-------+-------+-------+-------+-------+-------+
// 38-43  |  Σ+   |  1/x  | SQRT  |  LOG  |  LN   |  PRG  |
//           "A"     "B"     "C"     "D"     "E"     "@"  ;

void dmcpResetAutoOff(void) {
  // Key is ready -> clear auto off timer
  if(!key_empty() || (calcMode == cmTimerApp) || !getSystemFlag(FLAG_AUTOFF) || getSystemFlag(FLAG_RUNTIM) || programRunStop == PGM_RUNNING) {
    reset_auto_off();
  }
}



void dmcpCheckPowerStatus(void) {
  if(usb_powered() == 1) {
    setPowerStatus(psUsb);
  }
  else if(get_vbat() < 2500) {
    setPowerStatus(psBatteryLow);
    if(get_vbat() < 2000) {
      SET_ST(STAT_PGM_END);
    }
  }
  else {
    setPowerStatus(psBattery);
  }
}



void dmcpWaitForEvent(void) {
  if(nextTimerRefresh == 0) {
    // No timer so just wait until the next minute - this is unlikely to happen because the status bar
    // time will be updated every minute
    CLR_ST(STAT_CLK_WKUP_SECONDS);
    sys_sleep();
    return;
  }
  uint32_t lastCapturedTime = timeCurrentMs();
  timeCapture();
  uint32_t newCapturedTime = timeCurrentMs();
  if(newCapturedTime < lastCapturedTime) {
    // Time moved backwards (ON time < OFF time, or time set-up earlier by the user)
    timerStart(tidTimeUpdate, NOPARAM, 100);    // Restart TimeUpdate timer
    return;
  }
  // The current time is only updated once per main loop, so check how long since the last time update
  uint32_t elapsedTime = newCapturedTime - lastCapturedTime;    
  if(nextTimerRefresh <= elapsedTime) {
    // The next timer is already due
    return;
  }
  // Reduce nextTimerRefresh by the elapsed time
  nextTimerRefresh -= elapsedTime;
  // Try and optimise power usage by not using the timers if it can be avoided as they consume power
  // The wakeup for a SECOND or MINUTE timer does not consume very much power at all and is very
  // efficient, so use that wakeup if we can
  uint32_t timeToNextSecondMs = 1000 - (newCapturedTime % 1000);
  if(nextTimerRefresh <= 100 || nextTimerRefresh < timeToNextSecondMs) {
    powerDebugMark(M4);
    // Use the sys_timer because the timer will expire before the next second wakeup
    // Also allow 100ms for this function which is a conservative estimate
    const int TimerId = 0;
    sys_timer_start(TimerId, nextTimerRefresh);
    sys_sleep();
    sys_timer_disable(TimerId);
  }
  else {
    // The next timeout is after the next second or minute wakeup will happen
    // Use the MINUTE wakeup if we can, otherwise use the SECOND wakeup
    uint32_t timeToNextMinuteMs = 60000 - (newCapturedTime % 60000);
    if(nextTimerRefresh < timeToNextMinuteMs) {
      powerDebugMark(M5);
      SET_ST(STAT_CLK_WKUP_SECONDS);
    }
    else {
      powerDebugMark(M6);
      CLR_ST(STAT_CLK_WKUP_SECONDS);
    }
    sys_sleep();
    dmcpCheckPowerStatus();      // Regularly (second/minute) check the current powerstatus to update LOWBAT if necessary
  }
  lastCapturedTime = newCapturedTime;
  timeCapture();
  newCapturedTime = timeCurrentMs();
  if(newCapturedTime < lastCapturedTime) {
    timerRollOver();                            // Adjust all running timers for time roll-over at midnight
    return;
  }
}



void program_main(void) {
  int key = kcNoKey;
  int lastKey = kcNoKey;

  wp43MemInBlocks = 0;
  gmpMemInBytes = 0;
  mp_set_memory_functions(allocGmp, reallocGmp, freeGmp);

  key_to_alpha_table = alpha_upper_transl; //Remap the alpha keyboard layout for DMCP dialogs
  //SET_ST(STAT_ALPHA_TAB_Fn); // Alpha key table includes F keys - This doesn't apply to the WP43

  lcd_clear_buf();
  
// [DL - 2023/11/09] We assume that the WP43 keymap.bin has been applied and don't check anymore the keyboard layout
//  lcd_putsAt(t24, 4, "Press the bottom left key."); lcd_refresh();
//  while(key != 33 && key != 37) {
//    key = key_pop();
//    while(key == -1) {
//      sys_sleep();
//      key = key_pop();
//    }
//  }
//
//  wp43KbdLayout = (key == 37); // bottom left key
//  key = kcNoKey;

  lcd_clear_buf();
  timeCapture();
  configSetUpTimers();
  fnReset(CONFIRMED);
  refreshScreen();
  nextTimerRefresh = timerRun();

  #if 0
    longInteger_t li;
    uint32_t addr, min, max, *ptr;

    min = 1;
    max = 100000000;
    while(min+1 < max) {
      ptr = malloc((max + min) >> 1);
      if(ptr) {
        free(ptr);
        min = (max + min) >> 1;
      }
      else {
        max = (max + min) >> 1;
      }
    }

    ptr = malloc(min);
    xcopy(&addr, &ptr, 4);
    free(ptr);
    longIntegerInit(li);
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 16, 50);

    uIntToLongInteger(min, li);
    convertLongIntegerToShortIntegerRegister(li, 10, 51);

    ptr = (uint32_t *)qspi_user_addr();
    xcopy(&addr, &ptr, 4);
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 16, 52);

    addr = (uint32_t)qspi_user_size(); // QSPI user size in bytes
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 10, 53);

    ptr = (uint32_t *)&ram;
    xcopy(&addr, &ptr, 4);
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 16, 54);

    ptr = (uint32_t *)&indexOfItems;
    xcopy(&addr, &ptr, 4);
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 16, 55);

    ptr = (uint32_t *)ppgm_fp;
    xcopy(&addr, &ptr, 4);
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 16, 56);

    ptr = (uint32_t *)get_reset_state_file();
    xcopy(&addr, &ptr, 4);
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 16, 57);

    addr = 0x38; // RESET_STATE_FILE_SIZE;
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 10, 58);

    ptr = (uint32_t *)aux_buf_ptr();
    xcopy(&addr, &ptr, 4);
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 16, 59);

    addr = AUX_BUF_SIZE;
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 10, 60);

    ptr = (uint32_t *)write_buf_ptr();
    xcopy(&addr, &ptr, 4);
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 16, 61);

    addr = (uint32_t)write_buf_size();
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 10, 62);

    addr = (uint32_t)get_hw_id();
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 10, 63);

    ptr = (uint32_t *)resizeProgramMemory;
    xcopy(&addr, &ptr, 4);
    uIntToLongInteger(addr, li);
    convertLongIntegerToShortIntegerRegister(li, 16, 64);

    longIntegerFree(li);
  #endif // 1

  backToDMCP = false;

  lcd_refresh();

  // Status flags:
  //   ST(STAT_PGM_END)   - Indicates that program should go to off state (set by auto off timer)
  //   ST(STAT_SUSPENDED) - Program signals it is ready for off and doesn't need to be woken-up again
  //   ST(STAT_OFF)       - Program in off state (OS goes to sleep and only [EXIT] key can wake it up again)
  //   ST(STAT_RUNNING)   - OS doesn't sleep in this mode
  //   SET_ST(STAT_CLK_WKUP_SECONDS)
  SET_ST(STAT_CLK_WKUP_ENABLE); // Enable wakeup each minute (for clock update)
  SET_ST(STAT_RUNNING);

  while(!backToDMCP) {
    powerDebugMark(M3);
    if(ST(STAT_PGM_END)) {
      // Going to off mode
      lcd_set_buf_cleared(0); // Mark no buffer change region
      draw_power_off_image(1);
      LCD_power_off(0);

      SET_ST(STAT_SUSPENDED);
      CLR_ST(STAT_RUNNING);
      SET_ST(STAT_OFF);
      do {
        sys_sleep();
      } while(ST(STAT_PGM_END));
      CLR_ST(STAT_OFF);
      CLR_ST(STAT_SUSPENDED);
      SET_ST(STAT_RUNNING);

      LCD_power_on();
      rtc_wakeup_delay(); // Ensure that RTC readings after power off will be OK

      if(!lcd_get_buf_cleared()) {
        lcd_forced_refresh(); // Just redraw from LCD buffer
      }

      if(getSystemFlag(FLAG_AUTXEQ)) { // Run the program if AUTXEQ is set
        clearSystemFlag(FLAG_AUTXEQ);
        if(programRunStop != PGM_RUNNING) {
          screenUpdatingMode = SCRUPD_AUTO;
          runFunction(ITM_RS);
        }
        refreshScreen();
      }
    }
    else if(key_empty()) {
      // No keys available, wait for timers
      CLR_ST(STAT_RUNNING);
      dmcpWaitForEvent();
      timeCapture();
      if(ST(STAT_CLK_WKUP_FLAG)) {
        CLR_ST(STAT_CLK_WKUP_FLAG);
      }
      SET_ST(STAT_RUNNING);
    }

    if(ST(STAT_POWER_CHANGE)) {
      CLR_ST(STAT_POWER_CHANGE);
      dmcpCheckPowerStatus();
      lcd_refresh_dma();
      continue;
    }

    dmcpResetAutoOff();

    // Fetch the key
    //  < 0 -> No key event
    //  > 0 -> Key pressed
    // == 0 -> Key released
    key = key_pop();
    key = convertKeyCode(key);

    if(1 <= key && key <= 43) {
      powerDebugMark(M1);
      btnPressed(key);
      lastKey = key;
    }
    else if(key == KEY_SCREENSHOT) { // Virtual DMCP key code for screen-shot - simultaneous press of SHIFT+DISP (g+DISP on the WP43).
      fnScreenDump(NOPARAM); // Create screenshot 
      shiftG = false;        // Reset g Shift State (previously set when the g key was pressed before DISP)
    }
    else if(key == 0) { // Key released
      powerDebugMark(M2);
      btnReleased(lastKey);
    }
    else if (pendingScreenRefresh) {
        pendingScreenRefresh = false;
        refreshScreen();
    }

    // Execute pending timer jobs
    nextTimerRefresh = timerRun();
    lcd_refresh_dma();
  }
}

