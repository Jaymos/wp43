;*************************************************************
;*************************************************************
;**                                                         **
;**                     Riemann's zeta                      **
;**                                                         **
;*************************************************************
;*************************************************************
In: FL_SPCRES=0 FL_CPXRES=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnZeta



;=======================================
; zeta(Long Integer) --> Real
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"2"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"2" RX=Real:"1.644934066848226436472415166646025"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"3"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"3" RX=Real:"1.20205690315959428539973816151145"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"4"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=LonI:"4" RX=Real:"1.082323233711138191516003696541168"



;=======================================
; zeta(Time) --> Error24
;=======================================



;=======================================
; zeta(Date) --> Error24
;=======================================



;=======================================
; zeta(String) --> Error24
;=======================================
In:  FL_ASLIFT=0 RX=Stri:"String test"
Out: EC=24 FL_ASLIFT=0 RX=Stri:"String test"



;=======================================
; zeta(Real Matrix) --> Real Matrix
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=ReMa:"M2,3[2,3,4,5,6,7]"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=ReMa:"M2,3[2,3,4,5,6,7]" RX=ReMa:"M2,3[1.64493406684822643647241516664602518922,1.20205690315959428539973816151144999076,1.08232323371113819151600369654116790277,1.03692775514336992633136548645703416806,1.01734306198444913971451792979092052790,1.00834927738192282683979754984979675960]"



;=======================================
; zeta(Complex Matrix) --> Complex Matrix
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=CxMa:"M2,3[2,3,4,5,6,7]"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=CxMa:"M2,3[2,3,4,5,6,7]" RX=CxMa:"M2,3[1.64493406684822643647241516664602518922,1.20205690315959428539973816151144999076,1.08232323371113819151600369654116790277,1.03692775514336992633136548645703416806,1.01734306198444913971451792979092052790,1.00834927738192282683979754984979675960]"



;=======================================
; zeta(Short Integer) --> Error24
;=======================================



;=======================================
; zeta(Real) --> Real
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-3"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"-3" RX=Real:"0.008333333333333333333333333333333333"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-2"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"-2" RX=Real:"0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-1"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"-1" RX=Real:"-0.08333333333333333333333333333333333"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-0.5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"-0.5" RX=Real:"-0.2078862249773545660173067253970493"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"0"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0" RX=Real:"-0.5"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"0.5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"0.5" RX=Real:"-1.460354508809586812889499152515298"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"1"
Out: EC=5 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"1"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"1.5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"1.5" RX=Real:"2.612375348685488343348567567924072"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"2"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"2" RX=Real:"1.644934066848226436472415166646025"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"3"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"3" RX=Real:"1.20205690315959428539973816151145"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"4"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"4" RX=Real:"1.082323233711138191516003696541168"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"5"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Real:"5" RX=Real:"1.036927755143369926331365486457034"




;=======================================
; zeta(Complex) --> Complex
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=1 RX=Cplx:"0i1"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"0i1" RX=Cplx:".003300223685324102874217114210134566 i -.4181554491413216766892742398433611"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=1 RX=Cplx:"2i0"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"2i0" RX=Cplx:"1.644934066848226436472415166646025 i 0"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Cplx:"0i0"
Out: EC=0 FL_CPXRES=1 FL_ASLIFT=1 RL=Cplx:"0i0" RX=Cplx:"-0.5 i 0"
