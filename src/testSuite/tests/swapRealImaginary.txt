;*************************************************************
;*************************************************************
;**                                                         **
;**                 swap real imaginary                     **
;**                                                         **
;*************************************************************
;*************************************************************
In: FL_SPCRES=0 FL_CPXRES=0 SD=0 RM=0 IM=2compl SS=4 WS=64
Func: fnSwapRealImaginary



;=======================================
; swapRealImaginary(Long Integer) --> Error24
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=LonI:"5"
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=LonI:"5"



;=======================================
; swapRealImaginary(Time)
;=======================================



;=======================================
; swapRealImaginary(Date)
;=======================================



;=======================================
; swapRealImaginary(String) --> Error24
;=======================================
In:  FL_ASLIFT=0 RX=Stri:"String test"
Out: EC=24 FL_ASLIFT=0 RX=Stri:"String test"



;=======================================
; swapRealImaginary(Real Matrix)
;=======================================



;=======================================
; swapRealImaginary(Complex Matrix) --> Complex
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=CxMa:"M2,3[1i2,3i4,5i6,7i8,9i10,11i12]"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=CxMa:"M2,3[1i2,3i4,5i6,7i8,9i10,11i12]" RX=CxMa:"M2,3[2i1,4i3,6i5,8i7,10i9,12i11]"



;=======================================
; swapRealImaginary(Short Integer) --> Error24
;=======================================
In:  FL_ASLIFT=0 FL_CPXRES=0 RX=ShoI:"5#7"
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=ShoI:"5#7"



;=======================================
; swapRealImaginary(Real) --> Error24
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"0.0001"
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"0.0001"

In:  AM=GRAD FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"50"
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"50"

In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"89.99999"
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"89.99999"

In:  FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"5.32564":DMS
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"5.32564":DMS

In:  FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"-5.32564":GRAD
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"-5.32564":GRAD

; NaN
In:  FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"NaN":RAD
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"NaN":RAD

; Infinity
In:  FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=1 RX=Real:"inf":DEG
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"inf":DEG

In:  FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=0 RX=Real:"inf":MULTPI
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"inf":MULTPI

In:  FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=0 RX=Real:"-inf":RAD
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"-inf":RAD

; NaN
In:  FL_ASLIFT=0 FL_CPXRES=0 RX=Real:"NaN"
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"NaN"

; Infinity
In:  FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=1 RX=Real:"inf"
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"inf"

In:  FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=0 RX=Real:"inf"
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"inf"

In:  FL_ASLIFT=0 FL_CPXRES=0 FL_SPCRES=0 RX=Real:"-inf"
Out: EC=24 FL_CPXRES=0 FL_ASLIFT=0 RX=Real:"-inf"



;=======================================
; swapRealImaginary(Complex) --> Complex
;=======================================
In:  AM=DEG FL_ASLIFT=0 FL_CPXRES=0 RX=Cplx:"6.2 i -7.6"
Out: EC=0 FL_CPXRES=0 FL_ASLIFT=1 RL=Cplx:"6.2 i -7.6" RX=Cplx:"-7.6 i 6.2"
