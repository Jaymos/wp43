// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

/**
 * \file testSuite.h
 */
#if !defined(TESTSUITE_H)
  #define TESTSUITE_H

  #include "realType.h"
  #include "typeDefinitions.h"
  #include <stdint.h>

  #define FUNC_NOPARAM  0
  #define FUNC_CVT      1

  #define RE_ACCURATE   0
  #define RE_INACCURATE 1


  typedef struct {
    char     name[25];
    void     (*func)(uint16_t);
  } funcTest_t;

  void strToShortInteger        (char *nimBuffer, calcRegister_t regist);
  char *endOfString             (char *string);
  char hexToChar                (const char *string);
  void setParameter             (char *p);
  void inParameters             (char *token);
  void checkRegisterType        (calcRegister_t regist, char letter, uint32_t expectedDataType, uint32_t expectedTag);
  void checkExpectedOutParameter(char *p);
  void outParameters            (char *token);
  void functionToCall           (char *functionName);
  void processError             (const char *msg);
  void standardizeLine          (void);
  void processLine              (void);
  void processOneFile           (void);
  int  processTests             (const char *listPath);
  int  relativeErrorReal34      (real34_t *expectedValue34, real34_t *value34, char *numberPart, calcRegister_t regist, char letter);
  void wrongRegisterValue       (calcRegister_t regist, char letter, char *expectedValue);
  void expectedAndShouldBeValue (calcRegister_t regist, char letter, char *expectedValue, char *expectedAndValue);

#endif // !TESTSUITE_H
