// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

#include "hal/lcd.h"

#pragma GCC diagnostic ignored "-Wunused-parameter"

void setBlackPixel(uint32_t x, uint32_t y) {
}

void setWhitePixel(uint32_t x, uint32_t y) {
}

void flipPixel(uint32_t x, uint32_t y) {
}

void lcd_fill_rect(uint32_t x, uint32_t y, uint32_t dx, uint32_t dy, int val) {
}

void lcd_refresh(void) {
}
