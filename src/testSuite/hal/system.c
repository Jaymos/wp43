// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

#include "hal/system.h"

#include <stddef.h>

#pragma GCC diagnostic ignored "-Wunused-parameter"

void systemScreenshot(void) {
}



const char *systemMaker(void) {
  return NULL;
}



void systemProcessEvents(void) {
}



void systemQuit(void) {
}



uint32_t systemBatteryVoltage(void) {
  return 3100;
}
