// SPDX-License-Identifier: GPL-3.0-only
// SPDX-FileCopyrightText: Copyright The WP43 Authors

#include "wp43-gtk.h"
#include "apps/timerApp.h"
#include "config.h"
#include "core/memory.h"
#include "flags.h"
#include "gtkGui.h"
#include "hal/timer.h"
#include "items.h"
#include "longIntegerType.h"
#include "saveRestoreCalcState.h"
#include "ui/keyboard.h"
#include "ui/screen.h"
#include <assert.h>
#include <stdbool.h>

#include "wp43.h"

bool                calcLandscape;
bool                calcAutoLandscapePortrait;
GtkWidget           *screen;
GtkWidget           *frmCalc;
int16_t             screenStride;
uint32_t            *screenData;
bool                screenChange;
char                debugString[10000];
#if (DEBUG_REGISTER_L == 1)
  GtkWidget         *lblRegisterL1;
  GtkWidget         *lblRegisterL2;
#endif // (DEBUG_REGISTER_L == 1)
#if (SHOW_MEMORY_STATUS == 1)
  GtkWidget         *lblMemoryStatus;
#endif // (SHOW_MEMORY_STATUS == 1)

#if defined(EXPORT_ITEMS)
  int sortItems(void const *a, void const *b) {
    return compareString(a, b, CMP_EXTENSIVE);
  }
#endif // EXPORT_ITEMS

int main(int argc, char* argv[]) {
  assert(sizeof(dataBlock_t) == (1 << BITS_TO_SHIFT));
  #if defined(__APPLE__)
    // we take the directory where the application is as the root for this application.
    // in argv[0] is the application itself. We strip the name of the app by searching for the last '/':
    if(argc>=1) {
      char *curdir = malloc(1000);
      // find last /:
      char *s = strrchr(argv[0], '/');
      if(s != 0) {
        // take the directory before the appname:
        strncpy(curdir, argv[0], s-argv[0]);
        chdir(curdir);
        free(curdir);
      }
    }
  #endif // __APPLE__

  wp43MemInBlocks = 0;
  gmpMemInBytes = 0;
  mp_set_memory_functions(allocGmp, reallocGmp, freeGmp);

  calcLandscape             = false;
  calcAutoLandscapePortrait = true;

  for(int arg=1; arg<argc; arg++) {
    if(strcmp(argv[arg], "--landscape") == 0) {
      calcLandscape             = true;
      calcAutoLandscapePortrait = false;
    }

    if(strcmp(argv[arg], "--portrait") == 0) {
      calcLandscape             = false;
      calcAutoLandscapePortrait = false;
    }

    if(strcmp(argv[arg], "--auto") == 0) {
      calcLandscape             = false;
      calcAutoLandscapePortrait = true;
    }
  }

  #if defined(EXPORT_ITEMS)
    char name[LAST_ITEM][16], nameUtf8[25];
    int cat, nbrItems = 0;
    for(int i=1; i<LAST_ITEM; i++) {
      cat = indexOfItems[i].status & CAT_STATUS;
      if(cat == CAT_FNCT || cat == CAT_CNST || cat == CAT_SYFL || cat == CAT_RVAR) {
        strncpy(name[nbrItems++], indexOfItems[i].itemCatalogName, 16);
      }
    }
    qsort(name, nbrItems, 16, sortItems);
    printf("To be meaningfull, the list below must\n");
    printf("be displayed with the WP43_StandardFont!\n");
    for(int i=0; i<nbrItems; i++) {
      stringToUtf8(name[i], (uint8_t *)nameUtf8);
      printf("%s\n", nameUtf8);
    }
    exit(0);
  #endif // EXPORT_ITEMS

  gtk_init(&argc, &argv);
  configSetUpTimers();

  setupUI();

  restoreCalc();
  //ramDump();
  //refreshScreen();

  if(getSystemFlag(FLAG_AUTXEQ)) {
    clearSystemFlag(FLAG_AUTXEQ);
    if(programRunStop != PGM_RUNNING) {
      screenUpdatingMode = SCRUPD_AUTO;
      runFunction(ITM_RS);
    }
    refreshScreen();
  }

  gtk_main();

  return 0;
}
