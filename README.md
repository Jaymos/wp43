# WP43 Firmware
![WP43 simulator screenshot](res/artwork/WP43Example.png)

The *WP43* pocket calculator is a new fast, compact, reliable, and solid problem solver like you never owned before – instant on, fully programmable, incorporating a state-of-the-art high-resolution display, customizable by you, connecting to your computer, and RPN – a serious scientific instrument supporting you in your studies and professional activities. It readily provides advanced capabilities never before combined so conveniently in a true pocket-size calculator.

## Getting involved
- Regular announcements about the project are made on the [SwissMicros "Discuss!" forum](https://forum.swissmicros.com/viewforum.php?f=2).
- A simulator is available for download on the [Releases page](https://gitlab.com/wpcalculators/wp43/-/releases).
- Bugs and improvements can be reported on the [Issues page](https://gitlab.com/wpcalculators/wp43/-/issues).
- Read more about how to be involved in development on the [project Wiki](https://gitlab.com/wpcalculators/wp43/-/wikis/home).

## Status
- Specifications and manuals are mostly complete (please check the manuals being parts of the Releases).
- [SwissMicros](https://www.swissmicros.com) are developing the *WP43* hardware.
- Firmware development takes place using the [SwissMicros *DM42*](https://www.swissmicros.com/product/dm42) calculator as a platform and using a software simulator running under Windows, macOS, and Linux.
- The aim is to create a serious scientific instrument like we did with the [*WP 34S*](https://sourceforge.net/projects/wp34s/).

## Features
- Please see the [**_WP43 Owner's Manual_**](https://gitlab.com/rpncalculators/wp43/-/blob/master/docs/OwnersManual.pdf).

## Donations
If you want to support our work by donating some money to this project, please use this [link](https://www.paypal.com/cgi-bin/webscr?item_name=Donation+to+WP43&cmd=_donations&business=walter.bonin%40web.de&lc=us). Thankyou, hartstikke bedankt, mange tak(k), merci bien, mille grazie, muchas gracias, muito obrigado, suurkiitokset, tack så mycket, vielen Dank!
